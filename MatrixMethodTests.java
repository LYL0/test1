package utilities
import static org.junit.Assert.*;
class MatrixMethodTests{
  
  @Test
  public void checkDuplicate(){
    int[][]arr={{1,2,3},{4,5,6}};
    MatrixMtehod m = new MatrixMethod();
    int[][] result= m.duplicate(arr);
    int[][] expected={{1,2,3,1,2,3,},{4,5,6,4,5,6,}};
    assertEquals(expected,result);
  }
}