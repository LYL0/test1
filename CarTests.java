package tests;
/*Import statements*/
import static org.junit.Assert.*;
import vehicles.Car;
import java.util.*;

public class CarTests{
  /*this class contains the unit tests for the Car class.*/
  /*First we create an example car object.*/
  @Test
  public void accelerateTest(){
    Car c = new Car(50);
    c.accelerate();
    assertEquals(51,c.getSpeed());
  }
  @Test
  public void decelerateTest(){
    Car c = new Car(20);
    c.decelerate();
    assertEquals(19,c.getSpeed());
  }
  @Test
  public void GetTest(){
    Car c= new Car(40);
    assertEquals(40,c.getSpeed());
    assertEquals(0,c.getLocation());
  }
  @Test
  public void leftTest(){
    Car c = new Car(20);
    c.moveLeft();
    assertEquals(-20, c.getLocation());
    
  }
  @Test
    public void rightTest(){
    Car c = new Car(20);
    c.moveRight();
    assertEquals(20, c.getLocation());
    
  }
  @Test
  public void testConstructor(){
    try{
      Car c = new Car(-10);
      fail("An illegal argument exception should be thrown here,but it did not?");
    }
    catch(IllegalArgumentException e){
    }
    catch(Exception i){
      fail("An exception is thrown, but not the right type...");
    }
  }
  
}